#!/bin/bash

function run {
    if ! pgrep $1 ;
    then
        $@&
    fi
}

# run every restart
feh --bg-fill --randomize ~/Pictures/tapet/* &
xsetroot -cursor_name left_ptr &
xdg-settings set default-web-browser ungoogled-chromium.desktop &

# run if not running

# only UW
# run xrandr --output DVI-D-0 --off --output HDMI-0 --off --output DP-1 --off --output DP-0 --mode 3440x1440 --pos 0x0 --rotate normal --rate 144.00
# ST:UW
# run xrandr --output DVI-D-0 --off --output HDMI-0 --mode 1920x1080 --pos 0x0 --rotate normal --output DP-0 --mode 3440x1440 --pos 1920x0 --rotate normal --rate 144.00 --output DP-1 --off &
# only ST
# run xrandr --output DVI-D-0 --off --output HDMI-0 --mode 1920x1080 --pos 0x0 --rotate normal --output DP-0 -off --output DP-1 --off &

run sxhkd -c ~/.config/bspwm/sxhkd/sxhkdrc &

# run picom --config $HOME/.config/picom/picom.conf &
run picom --config $HOME/.config/picom/picom.conf --experimental-backends &
run pipewire &
run pipewire-pulse &
run dunst &
run redshift-gtk -l 41.74759:-74.08681 &
# run redshift -l 41.74759:-74.08681 &
run nm-applet &
# run unclutter --timeout 5 &
run unclutter -idle 5 &
run udiskie &
run wal -R &
# run xidlehook --not-when-fullscreen --timer 300 'betterlockscreen -l' '' --timer 600 'zzz'
# run amixer sset Master 25 unmute &

# applications
run firefox &
run Discord &
# run pulseeffects &
run easyeffects &
# run steam &

run alacritty -e bpytop
run alacritty -e nvtop

$HOME/.config/polybar/launch.sh &
