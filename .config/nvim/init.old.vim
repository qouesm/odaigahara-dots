au! BufWritePost $MYVIMRC source %  " auto source when writing to init.vm alternatively you can run :source $MYVIMRC

call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-commentary'                         " comments
Plug 'tpope/vim-fugitive'                           " git
Plug 'sheerun/vim-polyglot'                         " syntax highlighting
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
Plug 'justinmk/vim-sneak'                           " sneak
Plug 'jiangmiao/auto-pairs'                         " insert/delete brace pairs
Plug 'tpope/vim-surround'                           " surround
Plug 'vim-airline/vim-airline'                      " advanced status bar
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/goyo.vim'                            " focus mode
Plug 'junegunn/limelight.vim'                       " paragraph focus
Plug 'dylanaraps/wal.vim'                           " color scheme follows pywal
Plug 'morhetz/gruvbox'                              " gruvbox
Plug 'ap/vim-css-color'                             " highlights color chips  #ff0088
Plug 'lervag/vimtex'                                " latex; enables live preview
Plug 'kovetskiy/sxhkd-vim'                          " sxhkd niceties
Plug 'fatih/vim-go', { 'do': ':GoUpdateBinaries' }  " go
Plug 'Yggdroot/indentLine'                          " show vertical indent lines
Plug 'preservim/nerdtree'                           " file browser
Plug 'vim-autoformat/vim-autoformat'                " formatter
Plug 'turbio/bracey.vim', {'do': 'npm install --prefix server'}  " html live server
Plug 'joeytwiddle/sexy_scroller.vim'                " nice

Plug 'neoclide/coc.nvim', {'branch': 'release'} " COC

call plug#end()

colorscheme wal

" commentary
autocmd BufRead *.c   setlocal commentstring=//\ %s
autocmd BufRead *.php setlocal commentstring=//\ %s
autocmd BufRead *.a   setlocal commentstring=;\ %s
autocmd BufRead *.a   setlocal syntax=c

" md preview
let g:mkdp_auto_start = 0

" LaTex
" let g:tex_flavor  = 'latex'
" let g:tex_conceal = '0'
if has('nvim')
    let g:vimtex_compiler_progname = 'nvr'
endif
let g:vimtex_fold_manual = 1
let g:vimtex_view_method = 'mupdf'
let g:vimtex_compiler_latexmk = {
            \ 'options' : [
                \         '-output-directory=build',
                \     ]
                \ }

" Keys
let mapleader = "\<Space>"

" map <C-d> :Goyo \| set background=dark<CR>
" map <C-d> :Goyo<CR>
autocmd! User GoyoEnter Limelight
autocmd! User GoyoLeave Limelight!


" auto format on save; autoformat
au BufWrite * :Autoformat

" Basics
colorscheme wal
syntax enable               " Enables syntax highlighing
filetype plugin on
filetype indent on
set noerrorbells
set ignorecase
set smartcase
set undodir=~/.config/nvim/undodir
set undofile
set hidden                  " Required to keep multiple buffers open multiple buffers
set nowrap                  " Display long lines as just one line
set encoding=utf-8          " The encoding displayed::
set pumheight=10            " Makes popup menu smaller
set fileencoding=utf-8      " The encoding written to:: file
set ruler                   " Show the cursor position all the time
set cmdheight=2             " More space for displaying messages
set iskeyword+=-            " treat dash separated words as a word text object
set mouse=a                 " Enable your mouse
set splitbelow              " Horizontal splits will automatically be below
set splitright              " Vertical splits will automatically be to the right
set t_Co=256                " Support 256 colors
set conceallevel=0          " So that I can see `` in markdown files
set tabstop=4               " Insert 4 spaces for a tab
set softtabstop=4           " Insert 4 spaces for a tab
set shiftwidth=4            " Change the number of space characters inserted for indentation
set smarttab                " Makes tabbing smarter will realize you have 2 vs 4
set expandtab               " Converts tabs to spaces
set smartindent             " Makes indenting smart
set autoindent              " Good auto indent
set number                  " Line numbers
set relativenumber          " Relative line numbers
" set cursorline              " Enable highlighting of the current line
set background=dark         " tell vim what the background color looks like
set showtabline=2           " Always show tabs
set noshowmode              " We don't need to see things like -- INSERT -- anymore
set updatetime=300          " Faster completion
set timeoutlen=500          " By default timeoutlen is 1000 ms
set formatoptions-=cro      " Stop newline continution of comments
set clipboard=unnamedplus   " Copy paste between vim and everything else
set nocompatible            " from polyglot
set conceallevel=0          " don't allow plugins to hide `**` in *.md or `\textbf{}` in LaTeX

autocmd BufRead *.latex   set spell wrap linebreak conceallevel=0

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
" augroup ProjectDrawer
"   autocmd!
"   " autocmd VimEnter * :Vexplore
" augroup END
let g:netrq_banner = 0
nnoremap <C-f> :Vexplore<CR>

nnoremap <C-N> <C-W><C-H>
nnoremap <C-E> <C-W><C-J>
nnoremap <C-I> <C-W><C-K>
nnoremap <C-O> <C-W><C-L>


" COC


" Some servers have issues with backup files, see #649.
set nobackup
set nowritebackup

" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: There's always complete item selected by default, you may want to enable
" no select by `"suggest.noselect": true` in your configuration file.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
inoremap <silent><expr> <TAB>
            \ coc#pum#visible() ? coc#pum#next(1) :
            \ CheckBackspace() ? "\<Tab>" :
            \ coc#refresh()
inoremap <expr><S-TAB> coc#pum#visible() ? coc#pum#prev(1) : "\<C-h>"

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice.
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
            \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" GoTo code navigation.
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use K to show documentation in preview window.
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
    if (index(['vim','help'], &filetype) >= 0)
        execute 'h '.expand('<cword>')
    elseif (coc#rpc#ready())
        call CocActionAsync('doHover')
    else
        execute '!' . &keywordprg . " " . expand('<cword>')
    endif
endfunction

" Highlight the symbol and its references when holding the cursor.
autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
    autocmd!
    " Setup formatexpr specified filetype(s).
    autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
    " Update signature help on jump placeholder.
    autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
command! -nargs=0 Format :call CocAction('format')

" Add `:Fold` command to fold current buffer.
command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

" Add (Neo)Vim's native statusline support.
" NOTE: Please see `:h coc-status` for integrations with external plugins that
" provide custom statusline: lightline.vim, vim-airline.
" set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>
