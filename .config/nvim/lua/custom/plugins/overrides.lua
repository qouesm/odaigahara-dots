local M = {}

M.treesitter = {
  ensure_installed = {
    "vim",
    "lua",

    "html",
    "css",
    "scss",
    "javascript",
    "typescript",
    "svelte",
    "vue",
    "markdown",

    "latex",

    "python",

    "go",
    "gomod",

    "rust",

    "c",
    "make",
    "cmake",
    "ninja",

    "java",
    "kotlin",

    "sql",
    "graphql",

    "bash",
    "comment",
    "dockerfile",
    "gitattributes",
    "gitignore",
    "regex",

    "rasi",
    "sxhkdrc",
    "toml",
    "yaml",
  },
}

M.mason = {
  ensure_installed = {
    "vim-language-server",
    "lua-language-server",
    "selene",
    "stylua",

    "prettier",

    "html-lsp",
    "css-lsp",
    "tailwindcss-language-server",
    "typescript-language-server",
    "svelte-language-server",
    "vetur-vls",
    "rome",

    "marksman",
    "markdownlint",
    "vale",
    "texlab",

    -- lsp
    "python-lsp-server",
    -- linter
    "flake8",
    -- "pylint",
    "mypy", -- static typing
    "pydocstyle",
    "vulture", -- remove unused
    -- formatter
    "autopep8",
    "isort",

    "gopls",

    "rust-analyzer",

    "clangd",
    "jdtls",
    "cpplint",
    "clang-format",
    "kotlin-language-server",

    "sqlls",
    "sqlfluff",
    "sql-formatter",
    "graphql-language-service-cli",

    "shfmt",
    "bash-language-server",
    "shellcheck",
    "shellharden",

    "dockerfile-language-server",
    "hadolint",
  },
}

-- git support in nvimtree
M.nvimtree = {
  git = {
    enable = true,
  },

  renderer = {
    highlight_git = true,
    icons = {
      show = {
        git = true,
      },
    },
  },
}

return M
