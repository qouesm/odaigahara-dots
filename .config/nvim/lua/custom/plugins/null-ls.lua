local present, null_ls = pcall(require, "null-ls")

if not present then
  return
end

local b = null_ls.builtins

local sources = {
  b.diagnostics.selene,
  b.formatting.stylua,

  -- b.formatting.prettier.with { filetypes = { "html", "markdown", "css" } },
  b.formatting.prettier,
  b.formatting.rome,
  b.formatting.rustywind,

  b.diagnostics.vale,

  b.diagnostics.chktex.with { filetypes = { "tex", "latex" } },
  b.formatting.latexindent.with { filetypes = { "tex", "latex" } },

  b.diagnostics.flake8,
  -- b.diagnostics.pylint,
  b.diagnostics.mypy,
  b.diagnostics.pycodestyle,
  -- b.diagnostics.pydocstyle,
  b.diagnostics.vulture,
  b.formatting.autopep8,
  b.formatting.isort,

  b.formatting.gofmt,
  b.formatting.goimports,

  b.formatting.rustfmt,

  b.diagnostics.clang_check,
  b.diagnostics.cpplint,
  b.formatting.clang_format,

  b.diagnostics.sqlfluff.with { extra_args = { "--dialect", "postgres" } },
  -- b.formatting.sqlfluff.with { extra_args = { "--dialect", "postgres" } },
  b.formatting.sql_formatter.with { extra_args = { "-l", "postgres" } },

  b.diagnostics.shellcheck.with { diagnostics_format = "#{m} [#{c}]" },
  b.formatting.shfmt,
  b.formatting.shellharden,

  b.diagnostics.hadolint,
}

null_ls.setup {
  debug = true,
  sources = sources,
}
