local present, treesitter = pcall(require, "nvim-treesitter.configs")

if not present then
  return
end

treesitter.setup {
  ensure_installed = {
    "vim",
    "lua",

    "html",
    "css",
    "scss",
    "javascript",
    "typescript",
    "svelte",
    "vue",
    "markdown",

    "latex",

    "python",

    "go",
    "gomod",

    "rust",

    "c",
    "make",
    "cmake",
    "ninja",

    "java",
    "kotlin",

    "sql",
    "graphql",

    "bash",
    "comment",
    "dockerfile",
    "gitattributes",
    "gitignore",
    "regex",

    "rasi",
    "sxhkdrc",
    "toml",
    "yaml",
  },

  highlight = {
    enable = true,
    use_languagetree = true,
  },
}
