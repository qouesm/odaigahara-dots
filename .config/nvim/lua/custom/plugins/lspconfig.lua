local on_attach = require("plugins.configs.lspconfig").on_attach
local capabilities = require("plugins.configs.lspconfig").capabilities

local lspconfig = require "lspconfig"

local servers = {
  "vimls",
  "sumneko_lua",

  "html",
  "cssls",
  "tailwindcss",
  "tsserver",
  "svelte",
  "volar",
  "rome",

  "marksman",
  "texlab",

  "pylsp",
  -- "jedi_language_server",

  "gopls",

  "rust_analyzer",

  "clangd",
  "jdtls",

  "sqlls",
  "graphql",

  "bashls",

  "dockerls",
}

for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    on_attach = on_attach,
    capabilities = capabilities,
  }
end
