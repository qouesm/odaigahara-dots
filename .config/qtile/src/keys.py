import os
import subprocess

from libqtile.config import Click, Drag, Key
from libqtile.lazy import lazy

from src.util import kc
from libqtile import bar

alt = 'mod1'
ctl = 'control'
mod = 'mod4'
sft = 'shift'

my_term = 'alacritty'
my_launcher = 'rofi -show'

# resize functions
# def resize(qtile, direction):
#     layout = qtile.current_layout
#     child = layout.current
#     parent = child.parent

#     while parent:
#         if child in parent.children:
#             layout_all = False

#             if (direction == 'left' and parent.split_horizontal) or (
#                 direction == 'up' and not parent.split_horizontal
#             ):
#                 parent.split_ratio = max(
#                     5, parent.split_ratio - layout.grow_amount)
#                 layout_all = True
#             elif (direction == 'right' and parent.split_horizontal) or (
#                 direction == 'down' and not parent.split_horizontal
#             ):
#                 parent.split_ratio = min(
#                     95, parent.split_ratio + layout.grow_amount)
#                 layout_all = True

#             if layout_all:
#                 layout.group.layout_all()
#                 break

#         child = parent
#         parent = child.parent


# @lazy.function
# def resize_left(qtile):
#     current = qtile.current_layout.name
#     layout = qtile.current_layout
#     if current == 'bsp':
#         resize(qtile, 'left')
#     elif current == 'columns':
#         layout.cmd_grow_left()


# @lazy.function
# def resize_right(qtile):
#     current = qtile.current_layout.name
#     layout = qtile.current_layout
#     if current == 'bsp':
#         resize(qtile, 'right')
#     elif current == 'columns':
#         layout.cmd_grow_right()


# @lazy.function
# def resize_up(qtile):
#     current = qtile.current_layout.name
#     layout = qtile.current_layout
#     if current == 'bsp':
#         resize(qtile, 'up')
#     elif current == 'columns':
#         layout.cmd_grow_up()


# @lazy.function
# def resize_down(qtile):
#     current = qtile.current_layout.name
#     layout = qtile.current_layout
#     if current == 'bsp':
#         resize(qtile, 'down')
#     elif current == 'columns':
#         layout.cmd_grow_down()


# def backlight(action):
#     def f(qtile):
#         brightness = int(subprocess.run(
#             ['brightnessctl', 'g'], stdout=subprocess.PIPE).stdout)
#         max_brightness = int(subprocess.run(
#             ['brightnessctl', 'm'], stdout=subprocess.PIPE).stdout)
#         step = int(max_brightness / 10)

#         if action == 'inc':
#             if brightness < max_brightness - step:
#                 subprocess.run(['brightnessctl', 'set', str(
#                     brightness + step)], stdout=subprocess.PIPE).stdout
#             else:
#                 subprocess.run(['brightnessctl', 'set', str(
#                     max_brightness)], stdout=subprocess.PIPE).stdout
#         elif action == 'dec':
#             if brightness > step:
#                 subprocess.run(['brightnessctl', 'set', str(
#                     brightness - step)], stdout=subprocess.PIPE).stdout
#             else:
#                 subprocess.run(['brightnessctl', 'set', '0'],
#                                stdout=subprocess.PIPE).stdout
#     return f


# def show_keys():
#     key_help = ''
#     for k in keys:
#         mods = ''

#         for m in k.modifiers:
#             if m == 'mod4':
#                 mods += 'Super + '
#             else:
#                 mods += m.capitalize() + ' + '

#         if len(k.key) > 1:
#             mods += k.key.capitalize()
#         else:
#             mods += k.key

#         key_help += '{:<25} {}'.format(mods, k.desc + '\n')

#     return key_help


# keys.extend(
#     [
#         Key(
#             [mod],
#             'a',
#             lazy.spawn(
#                 'sh -c 'echo \''
#                 + show_keys()
#                 + '' | rofi -dmenu -theme ~/.config/rofi/hotkeys.rasi -i -p ''\''
#             ),
#             desc='Print keyboard bindings',
#         ),
#     ]
# )

keys = [
    # The essentials
    Key([mod], 'Return',
        lazy.spawn(my_term)),

    Key([mod], kc[1][4],
        lazy.spawn(my_launcher)),

    Key([mod, sft], kc[0][9],
        lazy.spawncmd()),

    Key([mod, ctl], kc[1][5],
        lazy.next_layout()),

    Key([mod], kc[0][0],
        lazy.window.kill()),

    Key([mod, ctl], kc[1][4],
        lazy.restart()),

    Key([mod, sft], kc[1][4],
        lazy.spawn('feh --bg-fill --randomize ~/Pictures/tapet/*')),

    Key([mod, ctl, sft], kc[0][0],
        lazy.shutdown()),

    # Key([mod], kc[0][6],
    #     lazy.spawn('betterscreenlock -l')),  # TODO doesn't work

    Key([mod], kc[0][3],
        lazy.hide_show_bar('all')),

    # layout changing
    Key([mod], kc[1][6],
        lazy.layout.left()),

    Key([mod], kc[1][7],
        lazy.layout.down()),

    Key([mod], kc[1][8],
        lazy.layout.up()),

    Key([mod], kc[1][9],
        lazy.layout.right()),

    Key([mod, sft], kc[1][6],
        lazy.layout.shuffle_left()),

    Key([mod, sft], kc[1][7],
        lazy.layout.shuffle_down()),

    Key([mod, sft], kc[1][8],
        lazy.layout.shuffle_up()),

    Key([mod, sft], kc[1][9],
        lazy.layout.shuffle_right()),

    Key([mod, sft], kc[1][5],
        lazy.layout.flip()),

    Key([mod], kc[2][6],
        lazy.layout.grow_left()),
    #  lazy.layout.shrink()),

    Key([mod], kc[2][7],
        lazy.layout.grow_down()),
    #  lazy.layout.shrink()),

    Key([mod], kc[2][8],
        lazy.layout.grow_up()),
    #  lazy.layout.grow()),

    Key([mod], kc[2][9],
        lazy.layout.grow_right()),
    #  lazy.layout.grow()),

    Key([mod, sft], kc[2][6],
        lazy.layout.flip_left()),

    Key([mod, sft], kc[2][7],
        lazy.layout.flip_down()),

    Key([mod, sft], kc[2][8],
        lazy.layout.flip_up()),

    Key([mod, sft], kc[2][9],
        lazy.layout.flip_right()),

    Key([mod], 'space',
        lazy.next_screen()),

    Key([mod, sft], 'space',
        lazy.to_screen()),

    Key([mod, ctl, sft], kc[2][5],
        lazy.window.toggle_floating()),

    # apps
    Key([mod], kc[0][5],
        lazy.spawn('firefox')),

    Key([mod], kc[0][6],
        lazy.spawn('Discord')),

    Key([mod], kc[0][7],
        lazy.spawn('pcmanfm')),

    Key([mod], kc[0][8],
        lazy.spawn(my_term + ' -e fff')),

    # system keys
    Key([], 'XF86AudioMute',
        lazy.spawn('pamixer -t')),

    Key([], 'XF86AudioLowerVolume',
        lazy.spawn('pamixer -d 5')),

    Key([], 'XF86AudioRaiseVolume',
        lazy.spawn('pamixer -i 5')),

    # Key([], 'XF86AudioPrev',
    #     lazy.spawn()),

    # Key([], 'XF86AudioPlay',
    #     lazy.spawn()),

    # Key([], 'XF86AudioNext',
    #     lazy.spawn()),

    Key([], 'XF86MonBrightnessDown',
        lazy.spawn('xbacklight -dec 5')),

    Key([], 'XF86MonBrightnessUp',
        lazy.spawn('xbacklight -inc 5')),

    Key([mod], 'p',
        lazy.spawn('arandr')),


    Key([], 'Print',
        lazy.spawn('flameshot gui')),

    # Key([], 'XF86AudioMedia',
    #     layout_switch()),

    # in progress
    Key([mod], 'F1',
        lazy.group['scratchpad'].dropdown_toggle('term')),

    # Key([mod], 'BackSpace',
    #     lazy.spawn('')),

    #  Key([mod], 'F1',  # nonfunctional
    #  lazy.spawn(init_qwerty)),

    #  Key([mod], 'F2',  # nonfunctional
    #  lazy.spawn(init_colemak)),
]

# group switching
for i in [
    kc[1][0],
    kc[1][1],
    kc[1][2],
    kc[1][3],
    kc[2][0],
    kc[2][1],
    kc[2][2],
    kc[2][3],
]:
    keys.extend([
        Key([mod], i,
            lazy.group[i].toscreen(toggle=False)),
        Key([mod, sft], i,
            lazy.window.togroup(i)),
    ])

# drag floating window
mouse = [
    Drag([mod], 'Button1',
         lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], 'Button2',
         lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], 'Button3',
          lazy.window.bring_to_front()),
]
