import subprocess

from libqtile import bar, widget
from libqtile.config import Screen

from src.util import colors, home
from src.widgets import *


def stretch():
    return widget.Spacer(length=bar.STRETCH)


def spacer():
    return widget.Spacer(length=sp)


def make_bar(main=False):
    widgets = [
        current_layout_icon,
        group_box,
        prompt,
        window_name,

        # stretch(),

        clock,

        stretch(),

        backlight,
        pulse_volume,
        *cpu,
        memory,
        *battery,
        systray if main else (),
        spacer() if main else (),
    ]

    # insert spacer every other address
    for i in range(1, len(widgets)*2-1, 2):
        widgets.insert(i, spacer())

    return bar.Bar(
        widgets=widgets,
        size=30,
        opacity=0.90,
    )


screens = [
    Screen(
        top=make_bar(main=True),
    ),
]

# more monitors
# for i in range(1):
#     screens.append(
#         Screen(
#             top=bar()
#         ),
#     )
