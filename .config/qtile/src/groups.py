from libqtile.config import Group, Key, Match, ScratchPad, DropDown
from libqtile.lazy import lazy

from src.keys import kc
from libqtile import bar

# Group names/keys
groups = [
    Group(kc[1][0], layout='Bsp', matches=[Match(wm_class='Steam')]),
    Group(kc[1][1], layout='Bsp'),
    Group(kc[1][2], layout='Bsp'),
    Group(kc[1][3], layout='Bsp'),
    Group(kc[2][0], layout='Bsp'),
    Group(kc[2][1], layout='Bsp', matches=[Match(wm_class='easyeffects')]),
    Group(kc[2][2], layout='Bsp', matches=[Match(title='BpyTOP')]),
    Group(kc[2][3], layout='Bsp', matches=[Match(wm_class='discord')]),
]

# scratchpad stuff

# groups.append(
#     ScratchPad('scratchpad', [
#         # define a drop down terminal.
#         # it is placed in the upper third of screen by default.
#         DropDown('term', my_term, height=0.75),
#     ])
# )

# groups.append(
#     ScratchPad(
#         "scratchpad",
#         [
#             DropDown(
#                 "term",
#                 "kitty",
#                 opacity=1,
#                 x=0.1,
#                 y=0.15,
#                 width=0.8,
#                 height=0.7,
#                 on_focus_lost_hide=True,
#             ),
#         ],
#     )
# )

