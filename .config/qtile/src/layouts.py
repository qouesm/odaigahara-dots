from libqtile import layout
from libqtile.config import Match

from src.util import colors
from libqtile import bar

layout_theme = {
    'border_width': 2,
    'margin': 4,
    'border_focus': colors[7],
    'border_normal': colors[0],
    'margin_on_single': 0,
    'grow_amount': 4,
}

layouts = [
    layout.Bsp(
        **layout_theme,
        fair=False,
        ratio=1.0,
    ),
    layout.Columns(
        **layout_theme,
        num_columns=3,
        insert_position=1
    ),
    layout.Max(**layout_theme),
    # layout.MonadTall(**layout_theme),
    # layout.Floating(**layout_theme),
]

# floating_layout = layout.Floating(
#     **layout_theme,
#     float_rules=[
#         *layout.Floating.default_float_rules,
#         Match(wm_class=''),
#     ],
# )
