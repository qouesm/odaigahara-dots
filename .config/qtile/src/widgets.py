import subprocess

from libqtile import widget

from src.util import colors, home

widget_defaults = dict(
    font='Source Code Pro Medium',
    fontsize=18,
    padding=12,
    update_interval=5,
    background=colors[0],
    foreground=colors[0],
)
extension_defaults = widget_defaults.copy()
sp = 8

backlight = widget.Backlight(
    backlight_name='intel_backlight',
    background=colors[3],
)

battery = (
    widget.GenPollText(
        update_interval=60,
        func=lambda: subprocess.check_output(
            ['bash', f'{home}/.config/qtile/script/batteryicon.sh']
        ).decode('utf-8'),
        background=colors[2],
    ),
    # widget.Battery(
    #     format='{char}{percent:2.0%}',
    #     charge_char=' ',
    #     discharge_char='',
    #     full_char='',
    #     unknown_char=' ',
    #     empty_char='',
    #     show_short_text=False,
    #     background=colors[2],
    #     fontsize=18,
    # ),
    widget.Battery(
        format='{hour:d}:{min:02d}',
        background=colors[2],
    ),
)

clock = widget.Clock(
    format='%a %d %H:%M',
    background=colors[1],
)

cpu = (
    widget.CPU(
        format=' {load_percent}%',
        background=colors[4],
    ),

    widget.CPUGraph(
        background=colors[4],
        graph_color=colors[0],
        fill_color=colors[0],
        border_color=colors[4],
        samples=100,
    ),

    widget.ThermalSensor(
        tag_sensor='Package id 0',
        background=colors[4],
        foreground=colors[0],
    ),
)

current_layout_icon = widget.CurrentLayoutIcon(
    scale=0.5,
    padding=4,
)

group_box = widget.GroupBox(
    padding=8,
    margin_x=8,
    active=colors[7],
    inactive=colors[0],
    disable_drag=True,
    rounded=False,
    highlight_method='block',

    # active screen; visible group
    this_current_screen_border=colors[1],
    # non-active screen; visible group
    this_screen_border=colors[4],
    # active screen; non-visible group
    other_current_screen_border=colors[8],
    # non-active screen; non-visible group
    other_screen_border=colors[8],
)

memory = widget.Memory(
    format=' {MemUsed:.0f}{mm}/16{mm}',
    measure_mem='G',
    background=colors[5],
)

prompt = widget.Prompt(
    background=colors[1],
    foreground=colors[0],
)

pulse_volume = widget.PulseVolume(
    update_interval=0.2,
    background=colors[6]
)

systray = widget.Systray()

window_name = widget.WindowName(
    foreground=colors[7],
    for_current_screen=True,
    max_chars=56,
)
