import os

from libqtile import bar
home = os.path.expanduser('~')
with open(home + '/.cache/wal/colors') as f:
    colors = f.read().splitlines()

# allows keyboard layout switching
# kc is a 2d list mapping to the keyboard grid
# keys is a symlink to either colemakdhmkeys or qwertykeys
# this is used in keys and also referenced in groups and the bar
kc = []
with open(home + '/.config/qtile/keys/keys') as f:
    for line in f:
        kc.append([str(n) for n in line.split()])
