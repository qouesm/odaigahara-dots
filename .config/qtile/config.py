# [silly ascii art that makes me look cooler than i really am]
import os
import subprocess

from typing import Callable, List

from libqtile import hook
from libqtile.command import lazy
# from libqtile.config import Click, Drag, Group, Key, Screen, ScratchPad, DropDown

from src.groups import groups
from src.keys import keys
from src.layouts import layouts
from src.screens import screens

# this should move eventually
from src.util import colors
widget_defaults = dict(
    font='Source Code Pro Medium',
    fontsize=18,
    padding=12,
    update_interval=5,
    background=colors[0],
    foreground=colors[0],
)

# base options
auto_fullscreen = True  # allows windows to fullscreen without asking
bring_front_click = True  # fronts windows on click
cursor_warp = True  # cursor follows focus
focus_on_window_activation = 'smart'  # autofocus new windows in current group
follow_mouse_focus = True  # autofocus where mouse is
wmname = 'LG3D'  # compat with java gui


# hooks
home = os.path.expanduser('~')


@hook.subscribe.startup_once
def start_once():
    subprocess.Popen([home + '/.config/qtile/script/autostart.sh'])


@hook.subscribe.startup
def restart():
    subprocess.Popen([home + '/.config/qtile/script/restart.sh'])
