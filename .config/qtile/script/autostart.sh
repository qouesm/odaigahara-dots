#!/bin/bash

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

# run amixer sset Master 25 unmute &
run dunst &
run nm-applet &
run picom --config $HOME/.config/picom/picom.conf --experimental-backends &
run pipewire &
run pipewire-pulse &
run redshift-gtk -l 41.74759:-74.08681 &
run udiskie &
run unclutter -idle 5 &
run wal -R &
# run xidlehook --not-when-fullscreen --timer 300 'betterlockscreen -l' '' --timer 600 'zzz'

# applications
firefox &
Discord &
$TERM -e bpytop &
